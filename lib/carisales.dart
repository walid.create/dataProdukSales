import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kasdigital_desktop/editdata.dart';
import 'package:kasdigital_desktop/input_produk.dart';

class CariSales extends StatefulWidget {
  List list;
  int index;

  CariSales({required this.list, required this.index});
  @override
  _CariSalesState createState() => _CariSalesState();
}

class _CariSalesState extends State<CariSales> {
  List<dynamic> datadata = [];
  @override
  initState() {
    // Text(widget.list[widget.index]['nama_sales']);
    // Text(widget.list[widget.index]['nama_produk']);
    // Text('Rp. ' + widget.list[widget.index]['harga_beli']);
    // Text('Stock: ' + widget.list[widget.index]['stok_produk']);
    // print(cariSales());
    super.initState();
  }

  // cariProdukSales() async {
  //   var url =
  //       "https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk_sales.php";
  //   await http.post(Uri.parse(url), body: {
  //     "a": widget.list[widget.index]['nama_sales'],
  //   });
  //   // var responses = await http.get(Uri.parse(
  //   //     'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk_sales.php'));
  //   // if (responses.statusCode == 200) {
  //   //   List<dynamic> jsonResponse = json.decode(responses.body);

  //   //   // print(jsonResponse[0]['nama']);
  //   //   // return jsonResponse;

  //   //   // //   String data_id_user = (jsonResponse[0]['nama']);
  //   //   setState(() {
  //   //     widget.list = jsonResponse;
  //   //   });
  //   //   return widget.list;
  //   // }
  // }

  Future<dynamic> cariSales() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk_sales.php?j=' +
            widget.list[widget.index]['id_sales']));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);

      // print(jsonResponse[0]['nama']);
      // return jsonResponse;

      // //   String data_id_user = (jsonResponse[0]['nama']);
      setState(() {
        widget.list = jsonResponse;
      });
      return jsonResponse;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              color: Colors.grey,
              width: double.infinity,
              child: FutureBuilder<dynamic>(
                  future: cariSales(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                        itemCount: widget.list.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                color: Colors.white,
                                child: Row(
                                  children: [
                                    Text(widget.list[index]['nama_sales']),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(widget.list[index]['nama_produk']),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text('Rp. ' +
                                        widget.list[index]['harga_beli']),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text('Stock: ' +
                                        widget.list[index]['stok_produk']),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    ElevatedButton(
                                        onPressed: () {}, child: Text('Edit'))
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                            ],
                          );
                        },
                      );
                    } else {
                      return const Text('loading');
                    }
                  }),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                  return InputProduk();
                }));
              },
              child: const Text('Back')),
          Text(widget.list[widget.index]['id_sales'])
        ],
      ),
    );
  }
}
