import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kasdigital_desktop/input_produk.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? selected2;
  List<dynamic> dataSupplierObjek = [];

  String namaNya = '';
  List<dynamic> dataNama = [];
  getData() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2:80/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_data_kas.php'));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);

      // print(jsonResponse[0]['nama']);
      // return jsonResponse;

      // //   String data_id_user = (jsonResponse[0]['nama']);
      setState(() {
        dataSupplierObjek = jsonResponse;
      });
      return jsonResponse;
    }

    @override
    void initState() {
      super.initState();
      getData();
    }
  }

  // dataKategori() async {
  //   // var url = 'https://www.googleapis.com/books/v1/volumes?q={http}';

  //   // Await the http get response, then decode the json-formatted response.
  //   // var response = await http.get(url.parse);
  //   var responses = await http.get(Uri.parse(
  //       'https://localhost/flutter/Flutter2/http/desktop_app_ver1/lib/php/viewKategori.php'));
  //   if (responses.statusCode == 200) {
  //     List<dynamic> jsonResponse = json.decode(responses.body);
  //     setState(() {
  //       dataKategoriObjek = jsonResponse;
  //     });
  //     return jsonResponse;
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    color: Colors.blue[700],
                    width: MediaQuery.of(context).size.width,
                    height: 20,
                    child: Container(
                      width: 300,
                      color: Colors.blue[700],
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                              color: Colors.blue[700],
                              child: Text(
                                'Menu',
                                style: TextStyle(color: Colors.white),
                              )),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                              color: Colors.blue[700],
                              child: Text(
                                'Account',
                                style: TextStyle(color: Colors.white),
                              )),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                              color: Colors.blue[700],
                              child: GestureDetector(
                                onTap: () => Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return InputProduk();
                                })),
                                child: const Text(
                                  'Sales',
                                  style: TextStyle(color: Colors.white),
                                ),
                              )),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 30,
              child: Container(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Container(
                          color: Colors.blue[800],
                          width: 250,
                          height: 150,
                          child: Image.asset('lib/gambar/db-logo.png'),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 250,
                          height: 150,
                          color: Colors.blue[800],
                          child: Image.asset('lib/gambar/graph.png'),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Container(
                          width: 250,
                          height: 150,
                          color: Colors.blue[200],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Text(
                      'DATA',
                      style: TextStyle(
                          color: Colors.red[800],
                          fontSize: 22,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Flexible(
                      child: FutureBuilder<dynamic>(
                          future: getData(),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
                              return ListView.builder(
                                itemCount: 1,
                                itemBuilder: (context, index) {
                                  return DropdownButton(
                                      value: selected2,
                                      items: dataSupplierObjek.map((list) {
                                        return DropdownMenuItem(
                                            value: list['nama'],
                                            child: Text(list['nama']));
                                      }).toList(),
                                      hint: Text('pilih kategori'),
                                      onChanged: (value) {
                                        if (mounted) {
                                          setState(() {
                                            selected2 = value.toString();
                                          });
                                        }
                                      });
                                },
                              );
                            } else {
                              return Text('no data');
                            }
                          }),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
