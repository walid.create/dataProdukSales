import 'dart:convert';

import 'package:http/http.dart' as http;

class ServicesDatabase {
  Future tambahKategori(
    String namaKategori,
  ) async {
    var url = Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/upload_kategori.php');
    await http.post(url, body: {
      'namaKategori': namaKategori,
    });
  }

  Future tambahProduk(String namaProduk, String? idKategori, String? idSales,
      String hargaBeliProduk, String hargaJualProduk, String stokProduk) async {
    var url = Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/upload_produk.php');
    await http.post(url, body: {
      'nama_produk': namaProduk,
      'id_kategori': idKategori,
      'id_sales': idSales,
      'harga_beli': hargaBeliProduk,
      'harga_jual': hargaJualProduk,
      'stok_produk': stokProduk,
    });
  }

  Future tambahSupplier(
    String namaSales,
    String alamatSales,
    String kontakSales,
  ) async {
    var url = Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/upload_supplier.php');
    await http.post(url, body: {
      'nama_sales': namaSales,
      'alamat_sales': alamatSales,
      'kontak_sales': kontakSales,
    });
  }

  // Future tambahTransaksi(String idTransaksi, String namaProduk,
  //     String customerInvoice, String quantityProduk) async {
  //   var url = Uri.parse(
  //       'https://192.168.1.2/flutter/Flutter2/http/desktop_pos/lib/php/upload_transaksi.php');
  //   await http.post(url, body: {
  //     'idTransaksi': idTransaksi,
  //     'namaProduk': namaProduk,
  //     'customerInvoice': customerInvoice,
  //     'quantityProduk': quantityProduk,
  //   });
  // }

  // Future tambahNomerInvoice(String idInvoice) async {
  //   var url = Uri.parse(
  //       'https://192.168.1.2/flutter/Flutter2/http/desktop_pos/lib/php/upload_nomer_invoice.php');
  //   await http.post(url, body: {
  //     'idInvoice': idInvoice,
  //   });
  // }

  // Future stokUpdate(String idProduk, String kategoriProduk, String namaProduk,
  //     String stokProduk, String waktu) async {
  //   var url = Uri.parse(
  //       'https://192.168.1.2/flutter/Flutter2/http/desktop_pos/lib/php/upload_stok.php');
  //   await http.post(url, body: {
  //     'idProduk': idProduk,
  //     'kategoriProduk': kategoriProduk,
  //     'namaProduk': namaProduk,
  //     'stokProduk': stokProduk,
  //     'waktu': waktu,
  //   });
  // }

  // Future lihatTransaksiPerInvoice(String idTransaksi) async {
  //   var url = Uri.parse(
  //       'https://192.168.1.2/flutter/Flutter2/http/desktop_pos/lib/php/viewTransaksiperInvoice.php');
  //   var result = await http.get(url);
  //   var jsonObject = json.decode(result.body);
  //   return jsonObject;
  // }
  Future lihatProdukbySales(int idSales) async {
    var url = Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/desktop_pos/lib/php/view_produk_sales.php');
    var result = await http.post(url, body: {
      'idSales': idSales,
    });
  }
}
