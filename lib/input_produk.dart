import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kasdigital_desktop/carisales.dart';
import 'package:kasdigital_desktop/editdata.dart';
import 'package:kasdigital_desktop/home.dart';
import 'package:kasdigital_desktop/services/database_services.dart';
import 'package:http/http.dart' as http;

class InputProduk extends StatefulWidget {
  const InputProduk({Key? key}) : super(key: key);

  @override
  _InputProdukState createState() => _InputProdukState();
}

class _InputProdukState extends State<InputProduk> {
  getKategori() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_kategori.php'));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);

      // print(jsonResponse[0]['nama']);
      // return jsonResponse;

      // //   String data_id_user = (jsonResponse[0]['nama']);
      setState(() {
        dataKategori = jsonResponse;
      });
      return jsonResponse;
    }
  }

  getProduk() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk.php'));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);
      List<String> dataProdukNama = [];
      List<String> dataProdukHarga = [];

      // for (int i = 0; i < jsonResponse.length; i++) {
      //   dataProdukNama.add(jsonResponse[i]['nama_produk']);
      //   dataProdukHarga.add(jsonResponse[i]['harga_beli']);
      //   setState(() {
      //     dataListProdukHarga = dataProdukHarga;
      //   });
      // }
      // // print(jsonResponse[0]['nama']);
      // // return jsonResponse;
      // for (int x = 0; x < dataProdukNama.length; x++) {
      //   setState(() {
      //     kategoriProdukSales
      //         .add(ProdukSales(dataProdukNama[x], dataProdukHarga[x]));
      //   });
      // }
      // // //   String data_id_user = (jsonResponse[0]['nama']);
      setState(() {
        dataProdukSales = jsonResponse;
      });
      return jsonResponse;
    }
  }

  @override
  void initState() {
    super.initState();
    getProduk();
  }

  TextEditingController inputkategori = TextEditingController();
  TextEditingController inputsales = TextEditingController();
  TextEditingController inputalamatsales = TextEditingController();
  TextEditingController inputkontaksales = TextEditingController();
  TextEditingController namaproduk = TextEditingController();
  TextEditingController hargabeliproduk = TextEditingController();
  TextEditingController hargajualproduk = TextEditingController();
  TextEditingController stokProduk = TextEditingController();
  final _formKeyKategori = GlobalKey<FormState>();
  final _formKeySales = GlobalKey<FormState>();
  final _formKeyProduk = GlobalKey<FormState>();
  String? selected2;
  String? selected3;
  String? selected4;
  String? selected5;
  int? selectedSales;
  String tesData = '';
  List<ProdukSales> kategoriProdukSales = [ProdukSales('item', 'items')];
  late int selected6;
  List<dynamic> dataKategori = [];
  List<dynamic> dataProduk = [];
  List<dynamic> dataSales = [];
  List<dynamic> dataProdukSales = [];
  List<dynamic> dataProdukSalesCari = [];
  List<String> dataListProdukNama = [];
  List<String> dataListProdukHarga = [];

  cariSales() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk_sales.php?j=' +
            tesData));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);

      // print(jsonResponse[0]['nama']);
      // return jsonResponse;

      // //   String data_id_user = (jsonResponse[0]['nama']);
      setState(() {
        dataProdukSalesCari = jsonResponse;
      });
      return jsonResponse;
    }
  }

//!  cari produk by sales buat halaman baru dengan meminta parameter id sales
  // cariProduk() async {
  //   var url =
  //       "https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk_sales.php";
  //   await http.post(Uri.parse(url), body: {
  //     "id": selected5,
  //   });
  // }

  getSales2() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_produk_sales.php'));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);
      setState(() {
        dataProdukSales = jsonResponse;
      });
      return jsonResponse;
    }
  }

  getSales() async {
    var responses = await http.get(Uri.parse(
        'https://192.168.1.2/flutter/Flutter2/http/kasdigital_desktop/lib/php/view_sales.php'));
    if (responses.statusCode == 200) {
      List<dynamic> jsonResponse = json.decode(responses.body);

      // print(jsonResponse[0]['nama']);
      // return jsonResponse;

      // //   String data_id_user = (jsonResponse[0]['nama']);
      setState(() {
        dataSales = jsonResponse;
      });
      return jsonResponse;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Expanded(
              child: Container(
                color: Colors.red,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: Colors.white,
                      width: MediaQuery.of(context).size.width,
                      height: 20,
                      child: Container(
                        width: 300,
                        color: Colors.blue[700],
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                color: Colors.blue[700],
                                child: GestureDetector(
                                  onTap: () =>
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                    return Home();
                                  })),
                                  child: Text(
                                    'Menu',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                )),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                                color: Colors.blue[700],
                                child: Text(
                                  'Account',
                                  style: TextStyle(color: Colors.white),
                                )),
                            SizedBox(
                              width: 10,
                            ),
                            Container(
                                color: Colors.blue[700],
                                child: const Text(
                                  'Sales',
                                  style: TextStyle(color: Colors.white),
                                )),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
                flex: 24,
                child: Container(
                  width: MediaQuery.of(context).size.width * 1,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 200,
                        color: Colors.white,
                        child: Form(
                            key: _formKeyKategori,
                            child: Column(
                              children: [
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value) =>
                                        value!.isEmpty ? ' harus di isi' : null,
                                    decoration: InputDecoration(
                                        hintText: 'nama Kategori'),
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: inputkategori,
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      if (_formKeyKategori.currentState!
                                          .validate()) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(SnackBar(
                                                content: Text(
                                                    'Kategori berhasil ditambahkan')));
                                      }
                                      ServicesDatabase()
                                          .tambahKategori(inputkategori.text);
                                      setState(() {
                                        inputkategori.text = '';
                                      });
                                    },
                                    child: Text('Save'))
                              ],
                            )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 300,
                        color: Colors.white,
                        child: Form(
                            key: _formKeySales,
                            child: Column(
                              children: [
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value1) => value1!.isEmpty
                                        ? ' harus di isi'
                                        : null,
                                    decoration:
                                        InputDecoration(hintText: 'nama Sales'),
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: inputsales,
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value2) => value2!.isEmpty
                                        ? ' harus di isi'
                                        : null,
                                    decoration: InputDecoration(
                                        hintText: 'alamat Sales'),
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: inputalamatsales,
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value3) => value3!.isEmpty
                                        ? ' harus di isi'
                                        : null,
                                    decoration: InputDecoration(
                                        hintText: 'No Kontak Sales'),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: inputkontaksales,
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      if (_formKeySales.currentState!
                                          .validate()) {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(SnackBar(
                                                content: Text(
                                                    'Sales berhasil ditambahkan')));
                                      }
                                      ServicesDatabase().tambahSupplier(
                                          inputsales.text,
                                          inputalamatsales.text,
                                          inputkontaksales.text);
                                      setState(() {
                                        inputsales.text = '';
                                        inputalamatsales.text = '';
                                        inputkontaksales.text = '';
                                      });
                                    },
                                    child: Text('Save'))
                              ],
                            )),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width * 0.3,
                        height: 400,
                        color: Colors.white,
                        child: Form(
                            key: _formKeyProduk,
                            child: Column(
                              children: [
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value1) => value1!.isEmpty
                                        ? ' harus di isi'
                                        : null,
                                    decoration: InputDecoration(
                                        hintText: 'nama Produk'),
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: namaproduk,
                                  ),
                                ),
                                Align(
                                  alignment: Alignment(-0.3, 1),
                                  child: Container(
                                    color: Colors.white,
                                    width: 200,
                                    height: 40,
                                    child: FutureBuilder<dynamic>(
                                        future: getKategori(),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return ListView.builder(
                                              itemCount: 1,
                                              itemBuilder: (context, index) {
                                                return DropdownButton(
                                                    isExpanded: false,
                                                    value: selected2,
                                                    items: dataKategori
                                                        .map((list) {
                                                      return DropdownMenuItem(
                                                          value: list[
                                                              'id_kategori'],
                                                          child: Text(list[
                                                              'nama_kategori']));
                                                    }).toList(),
                                                    hint:
                                                        Text('pilih kategori'),
                                                    onChanged: (value) {
                                                      if (mounted) {
                                                        setState(() {
                                                          selected2 =
                                                              value.toString();
                                                        });
                                                      }
                                                    });
                                              },
                                            );
                                          } else {
                                            return Text('loading');
                                          }
                                        }),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment(-0.3, 1),
                                  child: Container(
                                    color: Colors.white,
                                    width: 200,
                                    height: 40,
                                    child: FutureBuilder<dynamic>(
                                        future: getSales(),
                                        builder: (context, snapshot) {
                                          if (snapshot.hasData) {
                                            return ListView.builder(
                                              itemCount: 1,
                                              itemBuilder: (context, index) {
                                                return DropdownButton(
                                                    isExpanded: false,
                                                    value: selected3,
                                                    items:
                                                        dataSales.map((list) {
                                                      return DropdownMenuItem(
                                                          value:
                                                              list['id_sales'],
                                                          child: Text(list[
                                                              'nama_sales']));
                                                    }).toList(),
                                                    hint: Text('pilih sales'),
                                                    onChanged: (value) {
                                                      if (mounted) {
                                                        setState(() {
                                                          selected3 =
                                                              value.toString();
                                                        });
                                                      }
                                                    });
                                              },
                                            );
                                          } else {
                                            return Text('loading');
                                          }
                                        }),
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value3) => value3!.isEmpty
                                        ? ' harus di isi'
                                        : null,
                                    decoration:
                                        InputDecoration(hintText: 'harga beli'),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: hargabeliproduk,
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    decoration:
                                        InputDecoration(hintText: 'harga jual'),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: hargajualproduk,
                                  ),
                                ),
                                Container(
                                  width: 300,
                                  child: TextFormField(
                                    validator: (value3) => value3!.isEmpty
                                        ? ' harus di isi'
                                        : null,
                                    decoration: InputDecoration(
                                        hintText: 'Stok Produk'),
                                    keyboardType: TextInputType.number,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: stokProduk,
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                ElevatedButton(
                                    onPressed: () {
                                      if (_formKeyProduk.currentState!
                                          .validate()) {
                                        ServicesDatabase().tambahProduk(
                                            namaproduk.text,
                                            selected2,
                                            selected3,
                                            hargabeliproduk.text,
                                            hargajualproduk.text,
                                            stokProduk.text);
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(SnackBar(
                                                content: Text(
                                                    'produk berhasil ditambahkan')));
                                      }

                                      setState(() {
                                        namaproduk.text = '';
                                        selected2 = null;
                                        selected3 = null;
                                        hargabeliproduk.text = '';
                                        hargajualproduk.text = '';
                                        stokProduk.text = '';
                                      });
                                    },
                                    child: Text('Save'))
                              ],
                            )),
                      ),
                    ],
                  ),
                )),
            Expanded(
                flex: 16,
                child: Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height,
                  color: Colors.blueGrey,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Align(
                            alignment: Alignment(-0.3, 1),
                            child: Container(
                              margin: EdgeInsets.only(top: 5),
                              color: Colors.white,
                              width: 200,
                              height: 40,
                              child: FutureBuilder<dynamic>(
                                  future: getSales(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return ListView.builder(
                                        itemCount: 1,
                                        itemBuilder: (context, index) {
                                          return DropdownButton(
                                              isExpanded: false,
                                              value: selected5,
                                              items: dataSales.map((list) {
                                                return DropdownMenuItem(
                                                    value: list['id_sales'],
                                                    child: Text(
                                                        list['nama_sales']));
                                              }).toList(),
                                              hint: Text('Cari By sales'),
                                              onChanged: (value) {
                                                if (mounted) {
                                                  setState(() {
                                                    selected6 = int.parse(
                                                        value.toString());
                                                    tesData = value.toString();
                                                  });
                                                }
                                              });
                                        },
                                      );
                                    } else {
                                      return Text('loading');
                                    }
                                  }),
                            ),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          ElevatedButton(
                              onPressed: () {
                                cariSales();
                              },
                              child: Text('Cari')),
                          SizedBox(
                            width: 20,
                          ),
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return InputProduk();
                                }));
                              },
                              child: Text('Refresh')),
                        ],
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            Expanded(
                              child: FutureBuilder<dynamic>(
                                  future: getProduk(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return ListView.builder(
                                        itemCount: dataProdukSales.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                color: Colors.white,
                                                child: Row(
                                                  children: [
                                                    Text(dataProdukSales[index]
                                                        ['nama_sales']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(dataProdukSales[index]
                                                        ['nama_produk']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text('Rp. ' +
                                                        dataProdukSales[index]
                                                            ['harga_beli']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text('Stock: ' +
                                                        dataProdukSales[index]
                                                            ['stok_produk']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    ElevatedButton(
                                                        onPressed: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) {
                                                            return EditData(
                                                                list:
                                                                    dataProdukSales,
                                                                index: index);
                                                          }));
                                                        },
                                                        child: Text('Edit'))
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    } else {
                                      return Text('loading');
                                    }
                                  }),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Expanded(
                              child: FutureBuilder<dynamic>(
                                  future: cariSales(),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      return ListView.builder(
                                        itemCount: dataProdukSalesCari.length,
                                        itemBuilder: (context, index) {
                                          return Column(
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Container(
                                                color: Colors.white,
                                                child: Row(
                                                  children: [
                                                    Text(dataProdukSalesCari[
                                                        index]['nama_sales']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text(dataProdukSalesCari[
                                                        index]['nama_produk']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text('Rp. ' +
                                                        dataProdukSalesCari[
                                                                index]
                                                            ['harga_beli']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    Text('Stock: ' +
                                                        dataProdukSalesCari[
                                                                index]
                                                            ['stok_produk']),
                                                    SizedBox(
                                                      width: 10,
                                                    ),
                                                    ElevatedButton(
                                                        onPressed: () {
                                                          Navigator.push(
                                                              context,
                                                              MaterialPageRoute(
                                                                  builder:
                                                                      (context) {
                                                            return EditData(
                                                                list:
                                                                    dataProdukSalesCari,
                                                                index: index);
                                                          }));
                                                        },
                                                        child: Text('Edit'))
                                                  ],
                                                ),
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    } else {
                                      return Text('loading');
                                    }
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}

class ProdukSales {
  ProdukSales(this.name, this.price);
  final String name, price;
  @override
  String toString() => name;
}
